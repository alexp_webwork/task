var initArr = ['abc', 'c a b', ' ac  b ', ' bac', 'test2', 'c ba', 'a bc'];

// Первый вариант. Только for
function findUniq(initArr) {
	var helper = [];
	for (var i = 0; i < initArr.length; i++) {
		helper.push(initArr[i].replace(/\s+/g, '').split('').sort().join(''));
	}
	for (var i = 0; i < helper.length; i++) {
		if (helper[0] !== helper[i] && helper[i+1] !== undefined) {
			if (helper[i] !== helper[i+1]) {
				return initArr[i];
			} else {
				return initArr[0];
			}
		} else if (helper[0] !== helper[i] && helper[i+1] === undefined) {
			return initArr[i];
		}
	}
}

// Второй вариант. Не впечатлил по скорости
function findUniq(initArr) {
  var helper = [];

  for (var i = 0; i < initArr.length; i++) {
    helper.push(initArr[i].replace(/\s+/g, '').split('').sort().join(''));
  }
  
  var obj = helper.reduce(function(prev,cur,i){
        prev[cur] = {'count': (((prev[cur] && prev[cur].count) === undefined) ? 0 : prev[cur].count) + 1, 'index': i};
        return prev;
    },{});
  for (var key in obj) {
    if (obj[key].count === 1) return initArr[obj[key].index];
  }
}

// Третий вариант. Как по мне, самый элегантный
function findUniq(initArr) {
  var helper = {};

  search:
  for (var i = 0; i < initArr.length; i++) {
    var elem = initArr[i].replace(/\s+/g, '').split('').sort().join('');
      if (helper.hasOwnProperty(elem)) {
        ++helper[elem].count;
        continue search;
      }
    helper[elem] = {'count': 1, 'initStr': initArr[i]};
  }
  for (key in helper) {
    if (helper[key].count === 1) return helper[key].initStr;
  }
}

console.log(findUniq(initArr));
console.log(initArr);